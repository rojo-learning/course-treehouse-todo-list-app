
require 'rails_helper'


RSpec.describe 'Deleting todo items' do
  let!(:todo_list) do
    TodoList.create title: 'Grocery list', description: 'Groceries'
  end

  let!(:todo_item) do
    todo_list.todo_items.create content: 'Milk'
  end

  it 'is successful' do
    visit_todo_list(todo_list)
    within dom_id_for(todo_item) do
      click_link 'Delete'
    end

    expect(page).to have_content 'Todo list item was deleted.'
    expect(TodoItem.count).to eq(0)
  end
end
