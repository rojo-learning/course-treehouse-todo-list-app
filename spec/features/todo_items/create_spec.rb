
require 'rails_helper'

RSpec.describe 'Adding todo items' do
  let!(:todo_list) do
    TodoList.create title: 'Grocery list', description: 'Groceries'
  end

  it 'is successful with valid content' do
    visit_todo_list(todo_list)
    add_item 'Milk'

    within('div.flash') do
      expect(page).to have_content('Added todo list item.')
    end

    within('table.todo_items') do
      expect(page).to have_content('Milk')
    end
  end

  it 'displays an error with no content' do
    visit_todo_list(todo_list)
    add_item ''

    within('div.flash') do
      expect(page).to have_content('There was a problem adding that todo list item.')
    end

    expect(page).to have_content("Content can't be blank")
  end

  it 'displays an error with content less than 2 characters long' do
    visit_todo_list(todo_list)
    add_item 'B'

    within('div.flash') do
      expect(page).to have_content('There was a problem adding that todo list item.')
    end

    expect(page).to have_content("Content is too short")
  end
end
