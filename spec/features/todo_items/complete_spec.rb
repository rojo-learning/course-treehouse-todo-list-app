
require 'rails_helper'

RSpec.describe 'Mark todo items as complete' do
  let!(:todo_list) do
    TodoList.create title: 'Grocery list', description: 'Groceries'
  end

  let!(:todo_item) do
    todo_list.todo_items.create content: 'Milk'
  end

  it 'allows to mark a single item as complete' do
    expect(todo_item.completed_at).to be_nil

    visit_todo_list(todo_list)
    within dom_id_for(todo_item) do
      click_link 'Mark Complete'
    end
    todo_item.reload

    expect(todo_item.completed_at).not_to be_nil
  end

  context 'with completed items' do
    let!(:completed_item) do
      todo_list.todo_items.create content: 'Eggs', completed_at: 5.minutes.ago
    end

    it 'shows completed items as complete' do
      visit_todo_list(todo_list)

      within dom_id_for(completed_item) do
        expect(page).to have_content(completed_item.completed_at)
      end
    end

    it 'doesn\'t show the option to complete that item' do
      visit_todo_list(todo_list)

      within dom_id_for(completed_item) do
        expect(page).to_not have_content('Mark Complete')
      end
    end
  end
end
