
require 'rails_helper'

RSpec.describe 'Editing todo items' do
  let!(:todo_list) do
    TodoList.create title: 'Grocery list', description: 'Groceries'
  end

  let!(:todo_item) do
    todo_list.todo_items.create content: 'Milk'
  end

  it "is successful with valid content" do
    visit_todo_list(todo_list)
    within dom_id_for(todo_item) do
      click_link 'Edit'
    end
    fill_in 'Content', with: 'Lots of milk'
    click_button 'Save'

    expect(page).to have_content 'Saved todo list item.'
    todo_item.reload
    expect(todo_item.content).to eql 'Lots of milk'
  end

  it "is unsuccessful with no content" do
    visit_todo_list(todo_list)
    within dom_id_for(todo_item) do
      click_link 'Edit'
    end
    fill_in 'Content', with: ''
    click_button 'Save'

    expect(page).to have_content 'That todo item could not be saved.'
    expect(page).to have_content "Content can't be blank"
    expect(todo_item.content).to eql 'Milk'
  end

  it "is unsuccessful with no enough content" do
    visit_todo_list(todo_list)
    within dom_id_for(todo_item) do
      click_link 'Edit'
    end
    fill_in 'Content', with: 'C'
    click_button 'Save'

    expect(page).to have_content 'That todo item could not be saved.'
    expect(page).to have_content "Content is too short"
    expect(todo_item.content).to eql 'Milk'
  end

end
