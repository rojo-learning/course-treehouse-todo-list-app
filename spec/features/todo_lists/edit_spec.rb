
require 'rails_helper'

describe 'Editing todo lists' do
  let!(:todo_list) { TodoList.create title: 'Groceries', description: 'Grocery list.' }

  def update_todo_list(options = {})
    options[:title] ||= 'My todo list'
    options[:description] ||= 'This is my todo list.'

    todo_list = options[:todo_list]

    visit '/todo_lists'
    within dom_id_for(todo_list) do
      click_link 'Edit'
    end

    fill_in 'Title', with: options[:title]
    fill_in 'Description', with: options[:description]
    click_button 'Update Todo list'
  end

  it 'updates a todo lists successfully with correct information' do

    update_todo_list todo_list: todo_list,
                     title: 'My new title',
                     description: 'My new description'

    todo_list.reload

    expect(page).to have_content('Todo list was successfully updated')
    expect(todo_list.title).to eq('My new title')
    expect(todo_list.description).to eq('My new description')
  end

  it 'displays an error with no title' do
    update_todo_list todo_list: todo_list, title: ''
    expect(page).to have_content('error')
  end

  it 'displays an error with no description' do
    update_todo_list todo_list: todo_list, description: ''
    expect(page).to have_content('error')
  end

  it 'displays an error with a too short title' do
    update_todo_list todo_list: todo_list, title: 'Hi'
    expect(page).to have_content('error')
  end

  it 'displays an error with a too short description' do
    update_todo_list todo_list: todo_list, description: 'This'
    expect(page).to have_content('error')
  end
end
