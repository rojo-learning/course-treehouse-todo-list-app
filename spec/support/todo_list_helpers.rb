
module TodoListHelpers
  def visit_todo_list(list)
    visit '/todo_lists'
    within dom_id_for(list) do
      click_link 'List Items'
    end
  end

  def add_item(content)
    click_link 'New Todo Item'
    fill_in 'Content', with: content
    click_button 'Save'
  end
end
