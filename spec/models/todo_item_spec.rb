require 'rails_helper'

RSpec.describe TodoItem, :type => :model do
  it { should belong_to :todo_list }

  describe '#completed?' do
    let!(:item) { TodoItem.create content: 'Hello' }

    it 'is false then completed_at is blank' do
      expect(item.completed?).to be_falsey
    end

    it 'is true then completed_at is blank' do
      item.completed_at = Time.now
      expect(item.completed?).to be_truthy
    end
  end
end
